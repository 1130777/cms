package pt.isep.cms.students.shared;

public enum Gender {
	 MALE, 
	 FEMALE, 
	 UNSPECIFIED
}

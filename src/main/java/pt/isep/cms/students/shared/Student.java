package pt.isep.cms.students.shared;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Student implements Serializable {
	public String id;
  public String firstName;
  public String lastName;
  public Gender gender;
  public Date birthday;
	
	public Student() {}
	
	public Student(String id, String firstName, String lastName, Gender gender, Date birthday) {
		this.id = id;
	    this.firstName = firstName;
	    this.lastName = lastName;
	    this.gender = gender;
		this.birthday = birthday;
	}
	
	public StudentDetails getLightWeightStudent() {
	  return new StudentDetails(id, getFullName());
	}
	
  public String getId() { return id; }
  public void setId(String id) { this.id = id; }
  public String getFirstName() { return firstName; }
  public void setFirstName(String firstName) { this.firstName = firstName; }
  public String getLastName() { return lastName; }
  public void setLastName(String lastName) { this.lastName = lastName; }
  public Gender getGender() { return gender; }
  public void setGender(Gender gender) { this.gender = gender; }
  public String getGenderDesc() { return gender.toString(); }
  public Date getBirthday() { return birthday; }
  public void setBirthday(Date birthday) { this.birthday = birthday; }
  public String getFullName() { return firstName + " " + lastName; }
  public String getFullBirthDay() { return birthday.toString(); }
}

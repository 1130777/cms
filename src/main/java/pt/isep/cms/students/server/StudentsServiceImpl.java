package pt.isep.cms.students.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;

import pt.isep.cms.students.client.StudentsService;
import pt.isep.cms.students.shared.Gender;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.students.shared.StudentDetails;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SuppressWarnings("serial")
public class StudentsServiceImpl extends RemoteServiceServlet implements
    StudentsService {

  private static final String[] studentsFirstNameData = new String[] {
      "Hollie", "Emerson", "Healy", "Brigitte", "Elba", "Claudio",
      "Dena", "Christina", "Gail", "Orville", "Rae", "Mildred",
      "Candice", "Louise", "Emilio", "Geneva", "Heriberto", "Bulrush", 
      "Abigail", "Chad", "Terry", "Bell"};
  
  private final String[] studentsLastNameData = new String[] {
      "Voss", "Milton", "Colette", "Cobb", "Lockhart", "Engle",
      "Pacheco", "Blake", "Horton", "Daniel", "Childers", "Starnes",
      "Carson", "Kelchner", "Hutchinson", "Underwood", "Rush", "Bouchard", 
      "Louis", "Andrews", "English", "Snedden"};
  
  private final Gender[] studentsGenderData = new Gender[] {
	      Gender.FEMALE, Gender.MALE, Gender.MALE,
	      Gender.FEMALE, Gender.FEMALE, Gender.MALE,
	      Gender.FEMALE, Gender.FEMALE, Gender.MALE,
	      Gender.FEMALE, Gender.FEMALE, Gender.MALE,
	      Gender.FEMALE, Gender.FEMALE, Gender.MALE,
	      Gender.FEMALE, Gender.MALE, Gender.MALE,
	      Gender.FEMALE, Gender.MALE, Gender.MALE,
	      Gender.FEMALE};

  private final Date[] studentsDateData = new Date[22];
        
  private final HashMap<String, Student> students = new HashMap<String, Student>();

  public StudentsServiceImpl() {
    initStudents();
  }
  
  private void initStudents() {
    // TODO: Create a real UID for each student
    //
	 fillStudentsDate(); 
	  
    for (int i = 0; i < studentsFirstNameData.length && i < studentsLastNameData.length && i < studentsGenderData.length && i < studentsDateData.length; ++i) {
      Student student = new Student(String.valueOf(i), studentsFirstNameData[i], studentsLastNameData[i], studentsGenderData[i], studentsDateData[i]);
      students.put(student.getId(), student); 
    }
  }
  
  private void fillStudentsDate() {
	    // TODO: Create a real UID for each student
	    //
		  
	  	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
			studentsDateData[0] = df.parse("01/01/1993");
	        studentsDateData[1] = df.parse("02/02/1993");
	        studentsDateData[2] = df.parse("03/03/1993");
	        studentsDateData[3] = df.parse("04/04/1993");
	        studentsDateData[4] = df.parse("05/05/1993");
	        studentsDateData[5] = df.parse("06/06/1993");
	        studentsDateData[6] = df.parse("07/07/1993");
	        studentsDateData[7] = df.parse("08/08/1993");
			studentsDateData[8] = df.parse("09/09/1993");
	        studentsDateData[9] = df.parse("10/10/1993");
	        studentsDateData[10] = df.parse("11/11/1993");
	        studentsDateData[11] = df.parse("12/12/1993");
	        studentsDateData[12] = df.parse("13/11/1993");
	        studentsDateData[13] = df.parse("14/10/1993");
	        studentsDateData[14] = df.parse("15/09/1993");
	        studentsDateData[15] = df.parse("16/08/1993");
			studentsDateData[16] = df.parse("17/07/1993");
	        studentsDateData[17] = df.parse("18/06/1993");
	        studentsDateData[18] = df.parse("19/05/1993");
	        studentsDateData[19] = df.parse("20/04/1993");       
	        studentsDateData[20] = df.parse("21/03/1993");
	        studentsDateData[21] = df.parse("22/02/1993");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
  
  public Student addStudent(Student student) {
    student.setId(String.valueOf(students.size()));
    students.put(student.getId(), student); 
    return student;
  }

  public Student updateStudent(Student student) {
	  String lid=student.getId();
    students.remove(student.getId());
    students.put(student.getId(), student); 
    return student;
  }

  public Boolean deleteStudent(String id) {
    students.remove(id);
    return true;
  }
  
  public ArrayList<StudentDetails> deleteStudents(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteStudent(ids.get(i));
    }
    
    return getStudentDetails();
  }
  
  public ArrayList<StudentDetails> getStudentDetails() {
    ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();
    
    Iterator<String> it = students.keySet().iterator();
    while(it.hasNext()) { 
      Student student = students.get(it.next());          
      studentDetails.add(student.getLightWeightStudent());
    }
    
    return studentDetails;
  }

  public Student getStudent(String id) {
    return students.get(id);
  }
}

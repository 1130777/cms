package pt.isep.cms.contacts;

import com.google.gwt.event.shared.HandlerManager;

import pt.isep.cms.contacts.client.ContactsServiceAsync;
import pt.isep.cms.contacts.client.presenter.ContactsPresenter;
import pt.isep.cms.contacts.shared.ContactDetails;

import java.util.ArrayList;
import junit.framework.TestCase;

import static org.easymock.EasyMock.createStrictMock;

public class ExampleJRETest extends TestCase {
	private ContactsPresenter contactsPresenter;
	private ContactsServiceAsync mockRpcService;
	private HandlerManager eventBus;
	private ContactsPresenter.Display mockDisplay;

	protected void setUp() {
		mockRpcService = createStrictMock(ContactsServiceAsync.class);
		eventBus = new HandlerManager(null);
		mockDisplay = createStrictMock(ContactsPresenter.Display.class);
		contactsPresenter = new ContactsPresenter(mockRpcService, eventBus, mockDisplay);
	}

	public void testContactSort() {
		ArrayList<ContactDetails> contactDetails = new ArrayList<ContactDetails>();
		contactDetails.add(new ContactDetails("0", "Ferreira"));
		contactDetails.add(new ContactDetails("1", "Martins"));
		contactDetails.add(new ContactDetails("2", "Guimaraes"));
		contactsPresenter.setContactDetails(contactDetails);
		contactsPresenter.sortContactDetails();
		assertTrue(contactsPresenter.getContactDetail(0).getDisplayName().equals("Ferreira"));
		assertTrue(contactsPresenter.getContactDetail(1).getDisplayName().equals("Guimaraes"));
		assertTrue(contactsPresenter.getContactDetail(2).getDisplayName().equals("Martins"));
	}
}
